import './App.css'

import { Route, Routes, } from 'react-router-dom';

import Contact from './pages/contacto';
import Header from './components/navbar/header'
import Informacion from './components/informacion';
import Login from './pages/login';
import Portada from './components/utils/portada'
import ThemeProvider from "./components/utils/theme.context";

function App() {

  return (
    <ThemeProvider _theme="light">
      <div className="App">
        <Header />
        <Routes>
          <Route path='/' element={<Portada />} />
          <Route path='/login' element={<Login />} />
          <Route path='/contact' element={<Contact />} />
          <Route path='/info' element={<Informacion />} />
        </Routes>
      </div>
    </ThemeProvider>
  )
}

export default App
