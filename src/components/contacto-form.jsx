import { Field, Form, Formik } from 'formik';

import { ThemeContext } from './utils/theme.context';
import { useContext } from 'react';
const ContactForm = ({ onSubmit, innerRef }) => {
    const context = useContext(ThemeContext);
    const inicial_data = {
        name: '',
        e_mail: '',
        phone: '',
        message: ''
    };

    const submit = (values, actions) => {
        actions.setSubmitting(true)
        onSubmit(values);
        actions.resetForm();
        actions.setSubmitting(false)
    }

    const text_class = context.theme === "dark" ? "text-white" : "";

    return <Formik
        initialValues={inicial_data}
        onSubmit={submit}
        innerRef={innerRef} >
        {({ }) => {
            return <Form>
                <label className={text_class} >¿Desea contactarnos?</label>

                <div className="form-group">
                    <label className={text_class} htmlFor='name' >Nombre</label>
                    <Field
                        className="form-control"
                        id='name'
                        type='text'
                        name='name'
                        placeholder='Ingrese su nombre'

                    />
                </div>

                <div className='form-group' >
                    <label className={text_class} htmlFor='email' >e-Mail</label>
                    <Field

                        className='form-control'
                        id='email'
                        type='email'
                        name='e_mail'
                        placeholder='ejemplo@ejemplo.com'

                    />
                </div>

                <div className="form-group">
                    <label className={text_class} htmlFor='phone' >Telefono</label>
                    <Field
                        className="form-control"
                        id='phone'
                        type='text'
                        name='phone'
                        placeholder='+57 322-123-4567'

                    />
                </div>

                <div className="form-group">
                    <label className={text_class} htmlFor='mess' >Mensaje</label>
                    <Field
                        as='textarea'
                        className="form-control"
                        id='mess'
                        type='text'
                        name='message'
                        placeholder='Le escribo para...'

                    />
                </div>



            </Form>
        }}
    </Formik>
}

export default ContactForm;