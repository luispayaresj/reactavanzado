import { ThemeContext } from '../components/utils/theme.context';
import { useContext } from 'react';
function Informacion() {

    const context = useContext(ThemeContext);
    const text_class =
        context.theme === "dark" ? "text-white text-center" : "text-center";
    return (
        <div className={"container-fluid bg-" + context.theme}>
            <h1 className={text_class}>
                INFORMACION : Proyecto creado por Luis Payares Joly, CAR-IV
            </h1>
            <h4 className={text_class}>
                Lorem ipsum dolor sit amet. Eos voluptatem quia ea suscipit laboriosam et aspernatur neque At similique magni.
                Est galisum vitae At unde ullam aspernatur quaerat eum error quod ut Quis quia ut quia earum.
            </h4>
        </div>

    );
}
export default Informacion