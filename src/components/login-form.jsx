import { Field, Form, Formik } from 'formik';

import { ThemeContext } from './utils/theme.context';
import { useContext } from 'react';

const LoginForm = ({ onSubmit, innerRef }) => {
    const context = useContext(ThemeContext);
    const inicial_data = {
        user_name: '',
        password: ''
    };

    const submit = (values, actions) => {
        actions.setSubmitting(true)
        onSubmit(values);
        actions.resetForm();
        actions.setSubmitting(false)
    }

    const text_class = context.theme === "dark" ? "text-white" : "";

    return <Formik
        initialValues={inicial_data}
        onSubmit={submit}
        innerRef={innerRef} >
        {({ }) => {
            return <Form>
                <label className={text_class} >¿Desea ingresar al sistema?</label>
                <div className="form-group">
                    <label className={text_class} htmlFor='username' >
                        Nombre de usuario
                    </label>
                    <Field
                        className="form-control"
                        id='username'
                        type='text'
                        name='user_name'
                        placeholder='Ingrese su nombre de usuario'

                    />
                </div>

                <div className='form-group' >
                    <label className={text_class} htmlFor='pass' >Contraseña</label>
                    <Field
                        className='form-control'
                        id='pass'
                        type='password'
                        name='password'
                        placeholder='Ingrese su nombre de Contraseña'

                    />
                </div>



            </Form>
        }}
    </Formik>
}

export default LoginForm;