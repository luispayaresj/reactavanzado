import { Link } from 'react-router-dom';
import { ThemeContext } from '../utils/theme.context';
import { useContext } from 'react';
function Header() {
    const context = useContext(ThemeContext);
    return (
        <nav className='navbar navbar-expand-lg navbar-dark bg-dark bg-opacity-75'>
            <div className='container-fluid'>
                <Link className='navbar-brand badge bg-secondary text-wrap  fs-4 border border-4' to='/'>
                    PORTADA
                </Link>

            </div>
            <div className='container-fluid'>
                <Link className='navbar-brand badge bg-primary text-wrap  fs-4 border border-4' to='/login'>
                    Login
                </Link>

            </div>
            <div className='container-fluid'>
                <Link className='navbar-brand badge bg-success text-wrap  fs-4 border border-4' to='/contact'>
                    Contact
                </Link>

            </div>

            <div className='container-fluid'>
                <Link className='navbar-brand badge bg-danger text-wrap  fs-4 border border-4' to='/info'>
                    Information
                </Link>

            </div>
            <button
                className='btn btn-primary'

                onClick={() => {

                    context.toggle_theme("dark");
                }}

            >Theme</button>
        </nav>
    );
}

export default Header