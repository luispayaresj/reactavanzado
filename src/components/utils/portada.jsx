import { Banner } from '../navbar/banner'
import React from 'react';
import { ThemeContext } from './theme.context';
import { useContext } from 'react';
const Portada = () => {

    const context = useContext(ThemeContext);
    const text_class =
        context.theme === "dark" ? "text-white text-center" : "text-center";



    return (

        <div className={"container-fluid bg-" + context.theme}>
            <div>
                <Banner />
                <div>
                    <h2 className={text_class}>Primera entrega de react AVANZADO - CAR IV 2022</h2>

                </div>
            </div>
            <div className='px-5'>
                <div className='paragraph' >
                    <p className={text_class}>
                        Lorem ipsum dolor sit amet. Eos voluptatem quia ea suscipit laboriosam et aspernatur neque At similique magni.
                        Est galisum vitae At unde ullam aspernatur quaerat eum error quod ut Quis quia ut quia earum.

                        Vel possimus consequatur et quasi itaque ex facilis doloribus sed quos aliquid est provident officiis.
                        Ut nesciunt mollitia eos laboriosam sunt ut corrupti quae in debitis fugit aut reiciendis molestiae ut rerum
                        distinctio nam Quis ipsam. Sed repellat nisi ut voluptate eveniet cum vero unde id nesciunt iure.
                    </p>
                </div>
            </div>

        </div>


    )
}
export default Portada