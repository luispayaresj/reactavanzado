import { useContext, useRef } from 'react';

import ContactForm from '../components/contacto-form';
import React from 'react';
import { ThemeContext } from '../components/utils/theme.context';

const Contact = () => {
    const ref = useRef(null);

    const context = useContext(ThemeContext);

    const send = (values) => {
        console.log(values)

    }

    let ds = false;
    if (ref.current) {
        ds = ref.current.isSubmitting;
    }

    const text_class =
        context.theme === "dark" ? "text-white text-center" : "text-center";

    return <div className={"container-fluid bg-" + context.theme}>
        <div className='row'>
            <div className='col-6'
                style={{
                    backgroundSize: "cover",
                    backgroundImage: "url(https://picsum.photos/800/300)",
                }}
            >

            </div>
            <div className='col-6 py-5 '>
                <h2 className={text_class}>CONTACT</h2>
                <ContactForm onSubmit={send} innerRef={ref} />
                <button
                    className='btn btn-primary'
                    disabled={ds}
                    onClick={() => {
                        console.log("ok", ref.current);
                        if (ref.current) {
                            ref.current.submitForm();
                        }
                        //context.toggle_theme("dark");
                    }}

                >Send</button>
            </div>

        </div>

    </div>
}

export default Contact;