import { useContext, useRef } from 'react';

import LoginForm from '../components/login-form';
import React from 'react';
import { ThemeContext } from '../components/utils/theme.context';

const Login = () => {

    const ref = useRef(null);

    const context = useContext(ThemeContext);

    const login = (values) => {
        console.log(values)

    }

    let ds = false;
    if (ref.current) {
        ds = ref.current.isSubmitting;
    }

    const text_class =
        context.theme === "dark" ? "text-white text-center" : "text-center";
    const hm = localStorage.getItem('var')
    return <div className={"container-fluid bg-" + context.theme}>
        <div className='row'>
            <div className='col-6'
                style={{
                    backgroundSize: "cover",
                    backgroundImage: "url(https://picsum.photos/800/301)",
                }}
            >

            </div>
            <div className='col-6 py-5 '>
                <h2 className={text_class}>LOGIN</h2>
                <LoginForm onSubmit={login} innerRef={ref} />
                <button
                    className='btn btn-primary'
                    disabled={ds}
                    onClick={() => {
                        console.log("ok", ref.current);
                        if (ref.current) {
                            ref.current.submitForm();
                        }
                        //context.toggle_theme("dark");
                        localStorage.setItem('var', 'hola mundo')
                    }}

                >Login</button>
            </div>

        </div>

    </div>
}

export default Login;